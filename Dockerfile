FROM archlinux:latest

RUN pacman -Syu --noconfirm base-devel doxygen git python-breathe python-sphinx zsh
RUN git clone --depth=1 https://aur.archlinux.org/python-sphinx-bootstrap-theme && cd python-sphinx-bootstrap-theme && zsh -c 'EUID=1 makepkg -si --noconfirm' && cd .. && rm -rf python-sphinx-bootstrap-theme
